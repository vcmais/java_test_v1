## Code Challenge P4F

Você foi contratado para construir um **webservice REST** para o metrô de Londres. Você recebeu os arquivos em **[1]** para alimentar o banco de dados desse serviço.

1. Você deve criar um método para importar os arquivos existentes e uma estrutura para armazenar os dados, a importação deve ser feita apenas uma vez.

Ao finalizar a primeira parte, as seguintes funcionalidades foram pedidas pelo time de mobile para que eles possam construir uma aplicação para auxiliar no deslocamento dos passageiros.

1. Um método que liste um caminho (contendo todas as estações) qualquer entre a estação X e a estação Y

2. Um método que liste o menor caminho (contendo todas as estações) (considerando a quantidade de paradas como requisito para o menor caminho) entre a estação X e a estação Y

3. Um método que calcule o tempo aproximando da viagem no item 2, considerando que ao passar de uma estação adjacente à próxima, o passageiro gaste 3 minutos e ao trocar de linha gaste 12 minutos.

**Observações:**

1. Tanto o desenho da arquitetura do serviço assim como os testes unitários fazem parte da resolução do teste.
2. O retorno do webservice REST deve ser JSON
3. O código deve ser hospedado em um repositório forkado a partir desse.
4. Ao terminar o teste mande um email para code-challenge@vcmais.com com o link para seu repositório.

**Recursos: [1]**

[London Underground Geographic Maps - CSV](https://commons.wikimedia.org/wiki/London_Underground_geographic_maps/CSV) (para facilitar os arquivos foram inseridos no respositório.)